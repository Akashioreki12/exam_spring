package com.exam.examspring.Config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Bean
    public UserDetailsManager userDetailsManager() {
        UserDetails admin_personnel = User.withDefaultPasswordEncoder()
                .username("admin_personnel")
                .password("pass1")
                .roles("ADMIN1")
                .build();

        UserDetails admin_materiels = User.withDefaultPasswordEncoder()
                .username("admin_materiels")
                .password("Pass2")
                .roles("ADMIN2")
                .build();

        return new InMemoryUserDetailsManager(admin_personnel, admin_materiels);
    }


    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .authorizeRequests(authorizeRequests ->
                        authorizeRequests
                                .requestMatchers("/personnes/**").hasRole("ADMIN1")
                                .requestMatchers("/profs/**").hasRole("ADMIN1")
                                .requestMatchers("/employers/**").hasRole("ADMIN1")

                                .requestMatchers ("/depts/**").hasRole("ADMIN2")
                                .requestMatchers ("/bureaux/**").hasRole("ADMIN2")
                                .requestMatchers("/universities").hasRole("ADMIN2")
                                .requestMatchers ("/salles/**").hasRole("ADMIN2")

                                .anyRequest().authenticated()
                )
                .httpBasic(withDefaults())
                .formLogin(withDefaults());

        return http.build();
    }
}
