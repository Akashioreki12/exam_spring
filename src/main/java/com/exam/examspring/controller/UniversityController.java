package com.exam.examspring.controller;

import com.exam.examspring.model.University;
import com.exam.examspring.service.UniversityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/universities")
public class UniversityController {

    private final UniversityService universityService;

    @Autowired
    public UniversityController(UniversityService universityService) {
        this.universityService = universityService;

    }

    @GetMapping
    public List<University> getAllUniversities() {

        return universityService.getAllUniversities();
    }

    @GetMapping("/{id}")
    public University getUniversityById(@PathVariable Long id) {

        return universityService.getUniversityById(id);
    }

    @PostMapping
    public University createUniversity(@RequestBody University university) {

        return universityService.addUniversity(university);
    }

    @PutMapping("/{id}")
    public void updateUniversity(@PathVariable Long id, @RequestBody University university) {
        universityService.updateUniversity(id, university);
    }

    @DeleteMapping("/{id}")
    public void deleteUniversity(@PathVariable Long id) {
        universityService.deleteUniversity(id);
    }
}
