package com.exam.examspring.controller;

import com.exam.examspring.model.Departement;
import com.exam.examspring.service.DepartementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/depts")
public class DepartementController {
    private final DepartementService departmentService;

    @Autowired
    public DepartementController(DepartementService departmentService) {
        this.departmentService = departmentService;
    }

    @GetMapping
    public List<Departement> getAllDepartements() {
        return departmentService.getAllDept();
    }

    @GetMapping("/{id}")
    public Departement getDepartementById(@PathVariable Long id) {
        return departmentService.getDeptById(id);
    }

    @PostMapping
    public Departement createDepartement(@RequestBody Departement department) {
        return departmentService.addDept(department);
    }

    @PutMapping("/{id}")
    public void updateDepartement(@PathVariable Long id, @RequestBody Departement departement) {
        departmentService.updateDept(id,departement);
    }


    @DeleteMapping("/{id}")
    public void deleteDepartement(@PathVariable Long id) {
        departmentService.deleteDept(id);
    }
}
