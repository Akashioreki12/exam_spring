package com.exam.examspring.controller;

import com.exam.examspring.model.Salle;
import com.exam.examspring.service.SalleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/salles")
public class SalleController {

    private final SalleService salleService;

    @Autowired
    public SalleController(SalleService  salleService) {
        this.salleService = salleService;
    }

    @GetMapping
    public List<Salle> getAllSalles() {


        return salleService.getAllSalles( );
    }

    @GetMapping("/{id}")
    public Salle getSalleById(@PathVariable Long id) {


        return salleService.getSalleById(id);
    }


    @PostMapping
    public Salle createSalle(@RequestBody Salle salle) {


        return salleService.addSalle(salle);
    }




    @PutMapping("/{id}")
    public void updateNbSiegeSalle(@PathVariable Long id, @RequestBody Salle salle) {
        salleService.updateSalle(id, salle);
    }



    @DeleteMapping("/{id}")
    public void deleteSalle(@PathVariable Long id) {
        salleService.deleteSalle(id);
    }
}
