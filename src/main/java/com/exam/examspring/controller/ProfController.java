package com.exam.examspring.controller;

import com.exam.examspring.model.Personne;
import com.exam.examspring.model.Prof;
import com.exam.examspring.service.ProfService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/profs")
public class ProfController {
    private final ProfService profService;

    @Autowired
    public ProfController(ProfService profService) {
        this.profService = profService;
    }

    @GetMapping
    public List<Personne> getAllProfs() {
        return profService.getAllProfs();
    }

    @GetMapping("/{id}")
    public Prof getProfById(@PathVariable Long id) {
        return profService.getProfById(id);
    }

    @PostMapping
    public Prof createProf(@RequestBody Prof prof) {
        return profService.addProf(prof);
    }

    @PutMapping("/{id}")
    public void updateProf(@PathVariable Long id, @RequestBody Prof prof) {
        profService.updateProf(id, prof);
    }

    @DeleteMapping("/{id}")
    public void deleteProf(@PathVariable Long id) {
        profService.deleteProf(id);
    }
}
