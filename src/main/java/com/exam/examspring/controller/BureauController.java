package com.exam.examspring.controller;

import com.exam.examspring.model.Bureau;
import com.exam.examspring.service.BureauService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/bureaux")
public class BureauController {

    private final BureauService bureauService;

    @Autowired
    public BureauController(BureauService bureauService)
    {
        this.bureauService = bureauService;
    }

    @GetMapping
    public List<Bureau> getAllBureaux()   {
        return bureauService.getAllBureaux();
    }

    @GetMapping("/{id}")
    public Bureau getBureauById(@PathVariable Long id) {

        return bureauService.getBureauById(id);
    }

    @PostMapping
    public Bureau createBureau(@RequestBody Bureau bureau) {
        return bureauService.addBureau(bureau);



    }



    @DeleteMapping("/{id}")



    public void deleteBureau(@PathVariable Long id) {
        bureauService.deleteBureau(id);
    }
}
