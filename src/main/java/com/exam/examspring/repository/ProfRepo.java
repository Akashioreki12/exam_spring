package com.exam.examspring.repository;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfRepo extends PersonneRepo {
}
