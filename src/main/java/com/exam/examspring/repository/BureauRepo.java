package com.exam.examspring.repository;

import com.exam.examspring.model.Bureau;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BureauRepo extends JpaRepository<Bureau,Long> {
}
