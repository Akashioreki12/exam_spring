package com.exam.examspring.model;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
public class University {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long UniversityID;

    private String univNom;

    @OneToOne
    @JoinColumn(name = "personneID" , referencedColumnName = "personneID")
    private Personne personne;

    public University(String univNom) {
        this.univNom = univNom;
    }

    public University(Long universityID, String univNom) {
        UniversityID = universityID;
        this.univNom = univNom;
    }

    public University() {

    }

    public Long getUniversityID() {
        return UniversityID;
    }


    public String getUnivNom() {
        return univNom;
    }

    public void setUnivNom(String univNom) {
        this.univNom = univNom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        University that = (University) o;
        return Objects.equals(UniversityID, that.UniversityID) && Objects.equals(univNom, that.univNom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(UniversityID, univNom);
    }

    @Override
    public String toString() {
        return "University{" +
                "UniversityID=" + UniversityID +
                ", univNom='" + univNom + '\'' +
                '}';
    }
}
