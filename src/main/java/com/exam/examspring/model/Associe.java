package com.exam.examspring.model;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;

import java.util.Objects;

@Entity
@DiscriminatorValue("assoc")
public class Associe extends Prof {

    private Long assId;
    private String assNom;

    public Associe(Long profId, Long assId, String assNom) {
        super(profId);
        this.assId = assId;
        this.assNom = assNom;
    }

    public Associe(Long assId, String assNom) {
        this.assId = assId;
        this.assNom = assNom;
    }

    public Associe(String assNom) {
        this.assNom = assNom;
    }

    public Associe(Long personneID, String nom, Long profId, String profNom, String assNom) {
        super(personneID, nom, profId, profNom);
        this.assNom = assNom;
    }

    public Associe(String nom, Long profId, String profNom, String assNom) {
        super(nom, profId, profNom);
        this.assNom = assNom;
    }

    public Associe(Long profId, String profNom, String assNom) {
        super(profId, profNom);
        this.assNom = assNom;
    }

    public Associe(Long personneID, String nom, Long profId, String profNom, Long assId, String assNom) {
        super(personneID, nom, profId, profNom);
        this.assId = assId;
        this.assNom = assNom;
    }

    public Associe(String nom, Long profId, String profNom, Long assId, String assNom) {
        super(nom, profId, profNom);
        this.assId = assId;
        this.assNom = assNom;
    }

    public Associe(Long profId, String profNom, Long assId, String assNom) {
        super(profId, profNom);
        this.assId = assId;
        this.assNom = assNom;
    }

    public Associe() {

    }

    public Associe(Long profId, Long assId) {
        super(profId);
        this.assId = assId;
    }

    public Associe(Long assId) {
        this.assId = assId;
    }

    public Associe(Long personneID, String nom, Long profId, String profNom, Long assId) {
        super(personneID, nom, profId, profNom);
        this.assId = assId;
    }

    public Associe(String nom, Long profId, String profNom, Long assId) {
        super(nom, profId, profNom);
        this.assId = assId;
    }

    public Associe(Long profId, String profNom, Long assId) {
        super(profId, profNom);
        this.assId = assId;
    }

    public Long getAssId() {
        return assId;
    }



    public String getAssNom() {
        return assNom;
    }

    public void setAssNom(String assNom) {
        this.assNom = assNom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Associe associe = (Associe) o;
        return Objects.equals(assId, associe.assId) && Objects.equals(assNom, associe.assNom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), assId, assNom);
    }

    @Override
    public String toString() {
        return "Associe{" +
                "assId=" + assId +
                ", assNom='" + assNom + '\'' +
                '}';
    }
}
