package com.exam.examspring.model;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;

import java.util.Objects;

@Entity
@DiscriminatorValue("adjnt")
public class Adjoint extends Prof {

    private Long adjId;
    private String adjNom;

    public Adjoint(Long profId, Long adjId, String adjNom) {
        super(profId);
        this.adjId = adjId;
        this.adjNom = adjNom;
    }

    public Adjoint(Long adjId, String adjNom) {
        this.adjId = adjId;
        this.adjNom = adjNom;
    }

    public Adjoint(String adjNom) {
        this.adjNom = adjNom;
    }

    public Adjoint(Long personneID, String nom, Long profId, String profNom, String adjNom) {
        super(personneID, nom, profId, profNom);
        this.adjNom = adjNom;
    }

    public Adjoint(String nom, Long profId, String profNom, String adjNom) {
        super(nom, profId, profNom);
        this.adjNom = adjNom;
    }

    public Adjoint(Long profId, String profNom, String adjNom) {
        super(profId, profNom);
        this.adjNom = adjNom;
    }

    public Adjoint(Long personneID, String nom, Long profId, String profNom, Long adjId, String adjNom) {
        super(personneID, nom, profId, profNom);
        this.adjId = adjId;
        this.adjNom = adjNom;
    }

    public Adjoint(String nom, Long profId, String profNom, Long adjId, String adjNom) {
        super(nom, profId, profNom);
        this.adjId = adjId;
        this.adjNom = adjNom;
    }

    public Adjoint(Long profId, String profNom, Long adjId, String adjNom) {
        super(profId, profNom);
        this.adjId = adjId;
        this.adjNom = adjNom;
    }

    public Adjoint() {

    }

    public Adjoint(Long profId, Long adjId) {
        super(profId);
        this.adjId = adjId;
    }

    public Adjoint(Long adjId) {
        this.adjId = adjId;
    }

    public Adjoint(Long personneID, String nom, Long profId, String profNom, Long adjId) {
        super(personneID, nom, profId, profNom);
        this.adjId = adjId;
    }

    public Adjoint(String nom, Long profId, String profNom, Long adjId) {
        super(nom, profId, profNom);
        this.adjId = adjId;
    }

    public Adjoint(Long profId, String profNom, Long adjId) {
        super(profId, profNom);
        this.adjId = adjId;
    }

    public Long getAdjId() {
        return adjId;
    }


    public String getAdjNom() {
        return adjNom;
    }

    public void setAdjNom(String adjNom) {
        this.adjNom = adjNom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Adjoint adjoint = (Adjoint) o;
        return Objects.equals(adjId, adjoint.adjId) && Objects.equals(adjNom, adjoint.adjNom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), adjId, adjNom);
    }

    @Override
    public String toString() {
        return "Adjoint{" +
                "adjId=" + adjId +
                ", adjNom='" + adjNom + '\'' +
                '}';
    }
}
