package com.exam.examspring.model;

import jakarta.persistence.*;

import java.util.List;
import java.util.Objects;

@Entity
public class Departement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long deptID;

    private String deptNom;

    @ManyToOne
    @JoinColumn(name = "UniversityID" , referencedColumnName = "UniversityID")
    private University university;

    @OneToMany(mappedBy = "departement")
    private List<Prof> profs;


    public Departement(String deptNom) {
        this.deptNom = deptNom;
    }

    public Departement() {

    }

    public Departement(Long deptID, String deptNom) {
        this.deptID = deptID;
        this.deptNom = deptNom;
    }

    public Long getDeptID() {
        return deptID;
    }


    public String getDeptNom() {
        return deptNom;
    }

    public void setDeptNom(String deptNom) {
        this.deptNom = deptNom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Departement that = (Departement) o;
        return Objects.equals(deptID, that.deptID) && Objects.equals(deptNom, that.deptNom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deptID, deptNom);
    }

    @Override
    public String toString() {
        return "Departement{" +
                "deptID=" + deptID +
                ", deptNom='" + deptNom + '\'' +
                '}';
    }
}
