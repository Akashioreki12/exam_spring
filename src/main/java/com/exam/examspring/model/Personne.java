package com.exam.examspring.model;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name = "personnes")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE )
@DiscriminatorColumn(name = "type" , discriminatorType = DiscriminatorType.STRING ,length = 4)
@DiscriminatorValue("pers")
public class Personne {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long personneID;

    private String nom;

    public Personne() {
    }

    @OneToOne
    private University university;

    public Personne(Long personneID, String nom) {
        this.personneID = personneID;
        this.nom = nom;
    }

    public Personne(String nom) {
        this.nom = nom;
    }

    public Long getPersonneID() {
        return personneID;
    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Personne personne = (Personne) o;
        return Objects.equals(personneID, personne.personneID) && Objects.equals(nom, personne.nom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personneID, nom);
    }

    @Override
    public String toString() {
        return "Personne{" +
                "personneID=" + personneID +
                ", nom='" + nom + '\'' +
                '}';
    }
}
