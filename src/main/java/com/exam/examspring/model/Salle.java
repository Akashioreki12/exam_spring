package com.exam.examspring.model;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
public class Salle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long salleID;

    private int nbSiege;

    @ManyToOne
    @JoinColumn(name = "UniversityID" , referencedColumnName = "UniversityID")
    private University university;

    public Salle(Long salleID, int nbSiege) {
        this.salleID = salleID;
        this.nbSiege = nbSiege;
    }

    public Salle(int nbSiege) {
        this.nbSiege = nbSiege;
    }


    public Salle() {

    }

    public Long getSalleID() {
        return salleID;
    }


    public int getNbSiege() {
        return nbSiege;
    }

    public void setNbSiege(int nbSiege) {
        this.nbSiege = nbSiege;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Salle salle = (Salle) o;
        return nbSiege == salle.nbSiege && Objects.equals(salleID, salle.salleID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(salleID, nbSiege);
    }

    @Override
    public String toString() {
        return "Salle{" +
                "salleID=" + salleID +
                ", nbSiege=" + nbSiege +
                '}';
    }
}
