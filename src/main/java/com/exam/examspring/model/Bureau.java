package com.exam.examspring.model;


import jakarta.persistence.*;

import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "bureau")
public class Bureau {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long bureauID;

//    @OneToMany(mappedBy = "bureauList")
//    private List<Employer> employers;


    @ManyToOne
    @JoinColumn(name = "deptID" , referencedColumnName = "deptID")
    private Departement departement;

    @ManyToOne
    @JoinColumn(name = "UniversityID" , referencedColumnName = "UniversityID")
    private University university;

    public Bureau(Long bureauID) {
        this.bureauID = bureauID;
    }

    public Bureau() {
    }

    public Long getBureauID() {
        return bureauID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bureau bureau = (Bureau) o;
        return Objects.equals(bureauID, bureau.bureauID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bureauID);
    }

    @Override
    public String toString() {
        return "Bureau{" +
                "bureauID=" + bureauID +
                '}';
    }
}
