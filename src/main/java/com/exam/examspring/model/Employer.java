package com.exam.examspring.model;

import jakarta.persistence.*;

import java.util.List;

@Entity
@DiscriminatorValue("empl")
public class Employer extends Personne{


    private Long emplId;

    private String emplNom;

    @ManyToOne
    @JoinColumn(name = "bureauID" , referencedColumnName = "bureauID")
    private Bureau bureau;



    public Employer(Long emplId, String emplNom) {
        this.emplId = emplId;
        this.emplNom = emplNom;
    }

    public Employer(Long personneID, String nom, Long emplId, String emplNom) {
        super(personneID, nom);
        this.emplId = emplId;
        this.emplNom = emplNom;
    }

    public Employer(String nom, Long emplId, String emplNom) {
        super(nom);
        this.emplId = emplId;
        this.emplNom = emplNom;
    }

    public Employer(Long emplId) {
        this.emplId = emplId;
    }

    public Employer(Long personneID, String nom, Long emplId) {
        super(personneID, nom);
        this.emplId = emplId;
    }

    public Employer(String nom, Long emplId) {
        super(nom);
        this.emplId = emplId;
    }

    public Employer(String emplNom) {
        this.emplNom = emplNom;
    }

    public Employer(Long personneID, String nom, String emplNom) {
        super(personneID, nom);
        this.emplNom = emplNom;
    }

    public Employer(String nom, String emplNom) {
        super(nom);
        this.emplNom = emplNom;
    }

    public Employer() {

    }

    public String getEmplNom() {
        return emplNom;
    }

    public void setEmplNom(String emplNom) {
        this.emplNom = emplNom;
    }

    public Long getEmplId() {
        return emplId;
    }

    @Override
    public String toString() {
        return "Employer{" +
                "emplId=" + emplId +
                ", emplNom='" + emplNom + '\'' +
                '}';
    }


}
