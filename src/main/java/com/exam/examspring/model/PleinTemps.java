package com.exam.examspring.model;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;

import java.util.Objects;

@Entity
@DiscriminatorValue("pltmp")
public class PleinTemps extends Prof {
    private Long pltId;
    private String pltNom;

    public PleinTemps(Long profId, Long pltId, String pltNom) {
        super(profId);
        this.pltId = pltId;
        this.pltNom = pltNom;
    }

    public PleinTemps(Long pltId, String pltNom) {
        this.pltId = pltId;
        this.pltNom = pltNom;
    }

    public PleinTemps(String pltNom) {
        this.pltNom = pltNom;
    }

    public PleinTemps(Long personneID, String nom, Long profId, String profNom, String pltNom) {
        super(personneID, nom, profId, profNom);
        this.pltNom = pltNom;
    }

    public PleinTemps(String nom, Long profId, String profNom, String pltNom) {
        super(nom, profId, profNom);
        this.pltNom = pltNom;
    }

    public PleinTemps(Long profId, String profNom, String pltNom) {
        super(profId, profNom);
        this.pltNom = pltNom;
    }

    public PleinTemps(Long personneID, String nom, Long profId, String profNom, Long pltId, String pltNom) {
        super(personneID, nom, profId, profNom);
        this.pltId = pltId;
        this.pltNom = pltNom;
    }

    public PleinTemps(String nom, Long profId, String profNom, Long pltId, String pltNom) {
        super(nom, profId, profNom);
        this.pltId = pltId;
        this.pltNom = pltNom;
    }

    public PleinTemps(Long profId, String profNom, Long pltId, String pltNom) {
        super(profId, profNom);
        this.pltId = pltId;
        this.pltNom = pltNom;
    }

    public PleinTemps() {

    }

    public PleinTemps(Long profId, Long pltId) {
        super(profId);
        this.pltId = pltId;
    }

    public PleinTemps(Long pltId) {
        this.pltId = pltId;
    }

    public PleinTemps(Long personneID, String nom, Long profId, String profNom, Long pltId) {
        super(personneID, nom, profId, profNom);
        this.pltId = pltId;
    }

    public PleinTemps(String nom, Long profId, String profNom, Long pltId) {
        super(nom, profId, profNom);
        this.pltId = pltId;
    }

    public PleinTemps(Long profId, String profNom, Long pltId) {
        super(profId, profNom);
        this.pltId = pltId;
    }

    public Long getPltId() {
        return pltId;
    }


    public String getPltNom() {
        return pltNom;
    }

    public void setPltNom(String pltNom) {
        this.pltNom = pltNom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PleinTemps that = (PleinTemps) o;
        return Objects.equals(pltId, that.pltId) && Objects.equals(pltNom, that.pltNom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), pltId, pltNom);
    }

    @Override
    public String toString() {
        return "PleinTemps{" +
                "pltId=" + pltId +
                ", pltNom='" + pltNom + '\'' +
                '}';
    }
}
