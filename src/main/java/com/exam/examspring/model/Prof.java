package com.exam.examspring.model;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
@DiscriminatorValue("prof")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE )
@DiscriminatorColumn(name = "profType" , discriminatorType = DiscriminatorType.STRING ,length = 5)
public class Prof extends Personne{

    private Long profId;

    private String profNom;

    @ManyToOne
    @JoinColumn(name = "deptID" , referencedColumnName = "deptID")
    private Departement departement;

    public Prof(Long profId) {
        this.profId = profId;
    }

    public Prof() {

    }

    public Prof(Long personneID, String nom, Long profId, String profNom) {
        super(personneID, nom);
        this.profId = profId;
        this.profNom = profNom;
    }

    public Prof(String nom, Long profId, String profNom) {
        super(nom);
        this.profId = profId;
        this.profNom = profNom;
    }

    public Prof(Long profId, String profNom) {
        this.profId = profId;
        this.profNom = profNom;
    }

    public Long getProfId() {
        return profId;
    }


    public String getProfNom() {
        return profNom;
    }

    public void setProfNom(String profNom) {
        this.profNom = profNom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Prof prof = (Prof) o;
        return Objects.equals(profId, prof.profId) && Objects.equals(profNom, prof.profNom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), profId, profNom);
    }

    @Override
    public String toString() {
        return "Prof{" +
                "profId=" + profId +
                ", profNom='" + profNom + '\'' +
                '}';
    }
}

