package com.exam.examspring.service;

import com.exam.examspring.model.Personne;
import com.exam.examspring.model.Prof;
import com.exam.examspring.repository.ProfRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProfService {

    private final ProfRepo profRepo;

    @Autowired
    public ProfService(@Qualifier("profRepo") ProfRepo profRepo) {
        this.profRepo = profRepo;
    }

    public List<Personne> getAllProfs() {
        return profRepo.findAll();
    }

    public Prof getProfById(Long id) {
        return (Prof) profRepo.findById(id).orElse(null);
    }

    public void updateProf(Long id, Prof prof) {
        profRepo.findById(id)
                .map(existingProf -> {
                    existingProf.setNom(prof.getProfNom());
                    return profRepo.save(existingProf);
                })
                .orElseGet(() -> profRepo.save(prof));
    }

    public Prof addProf(Prof prof) {
        return profRepo.save(prof);
    }

    public void deleteProf(Long id) {
        profRepo.deleteById(id);
    }
}
