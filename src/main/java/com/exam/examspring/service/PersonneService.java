package com.exam.examspring.service;

import com.exam.examspring.model.Personne;
import com.exam.examspring.repository.PersonneRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonneService {

    private final PersonneRepo personneRepo;

    @Autowired
    public PersonneService(@Qualifier("personneRepo") PersonneRepo personneRepo) {
        this.personneRepo = personneRepo;
    }

    public List<Personne> getAllPersonnes() {
        return personneRepo.findAll();
    }

    public Personne getPersonneById(Long id) {
        return personneRepo.findById(id).orElse(null);
    }

    public Personne addPersonne(Personne personne) {
        return personneRepo.save(personne);
    }

    public void deletePersonne(Long id) {
        personneRepo.deleteById(id);
    }

    public void updatePersonne(Long id, Personne pers) {
        personneRepo.findById(id)
                .map(existingPersonne -> {
                    existingPersonne.setNom(pers.getNom());
                    return personneRepo.save(existingPersonne);
                })
                .orElseGet(() -> personneRepo.save(pers));
    }
}
