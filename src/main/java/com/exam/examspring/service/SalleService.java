package com.exam.examspring.service;

import com.exam.examspring.model.Salle;
import com.exam.examspring.repository.SalleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SalleService {
    private final SalleRepo salleRepo;

    @Autowired
    public SalleService(SalleRepo salleRepo) {
        this.salleRepo = salleRepo;
    }

    public List<Salle> getAllSalles() {
        return salleRepo.findAll();
    }

    public Salle getSalleById(Long id) {
        return salleRepo.findById(id).orElse(null);
    }

    public Salle addSalle(Salle salle) {
        return salleRepo.save(salle);
    }

    public void deleteSalle(Long id) {
        salleRepo.deleteById(id);
    }

    public void updateSalle(Long id, Salle salle) {
        salleRepo.findById(id)
                .map(existingSalle -> {
                    existingSalle.setNbSiege(salle.getNbSiege());
                    return salleRepo.save(existingSalle);
                })
                .orElseGet(() -> salleRepo.save(salle));
    }
}
