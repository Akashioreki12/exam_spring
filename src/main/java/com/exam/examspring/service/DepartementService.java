package com.exam.examspring.service;

import com.exam.examspring.model.Departement;
import com.exam.examspring.repository.DepartementRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartementService {
    private final DepartementRepo departementRepo;


    public DepartementService( DepartementRepo departementRepo) {
        this.departementRepo = departementRepo;
    }

    public List<Departement> getAllDept(){
        return departementRepo.findAll();
    }

    public Departement getDeptById(Long id){
        return departementRepo.findById(id).orElse(null);


    }

    public Departement addDept(Departement dept){

        return departementRepo.save(dept);
    }

    public void deleteDept(Long id){

        departementRepo.deleteById(id);

    }

    public void updateDept(Long id, Departement dept){
        departementRepo.findById(id)
                .map(existingDept -> {
                    existingDept.setDeptNom(dept.getDeptNom());
                    return departementRepo.save(existingDept);
                })
                .orElseGet(() -> departementRepo.save(dept));
    }


}
