package com.exam.examspring.service;

import com.exam.examspring.model.Bureau;
import com.exam.examspring.repository.BureauRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BureauService {
    private final BureauRepo bureauRepo;

    @Autowired
    public BureauService(BureauRepo bureauRepo) {
        this.bureauRepo = bureauRepo;
    }

    public List<Bureau> getAllBureaux() {
        return bureauRepo.findAll();
    }

    public Bureau getBureauById(Long id) {
        return bureauRepo.findById(id).orElse(null);
    }

    public Bureau addBureau(Bureau bureau) {
        return bureauRepo.save(bureau);
    }

    public void deleteBureau(Long id) {
        bureauRepo.deleteById(id);
    }


}
