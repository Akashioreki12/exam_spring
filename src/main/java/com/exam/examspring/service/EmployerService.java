package com.exam.examspring.service;

import com.exam.examspring.model.Employer;
import com.exam.examspring.model.Personne;
import com.exam.examspring.repository.EmployerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployerService {

    private final EmployerRepo employerRepo;

    @Autowired
    public EmployerService(@Qualifier("employerRepo") EmployerRepo employerRepo) {

        this.employerRepo = employerRepo;
    }

    public List<Personne> getAllEmployers() {
        return employerRepo.findAll();
    }

    public Personne getEmployerById(Long id) {
        return employerRepo.findById(id).orElse(null);
    }

    public Employer addEmployer(Employer employer) {

        return employerRepo.save(employer);
    }

    public void deleteEmployer(Long id) {employerRepo.deleteById(id);
    }

    public void updateEmployer(Long id, Employer employer) {
        employerRepo.findById(id)
                .map(existingEmployer -> {
                    existingEmployer.setNom(employer.getEmplNom());
                    return employerRepo.save(existingEmployer);
                })
                .orElseGet(() -> employerRepo.save(employer));
    }
}
