package com.exam.examspring.service;

import com.exam.examspring.model.University;
import com.exam.examspring.repository.UniversityRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UniversityService {
    private final UniversityRepo universityRepo;

    @Autowired
    public UniversityService(UniversityRepo universityRepo) {
        this.universityRepo = universityRepo;
    }

    public List<University> getAllUniversities() {
        return universityRepo.findAll();
    }

    public University getUniversityById(Long id) {
        return universityRepo.findById(id).orElse(null);
    }
    public University addUniversity(University university) {
        return universityRepo.save(university);
    }



    public void deleteUniversity(Long id) {
        universityRepo.deleteById(id);
    }
    public void updateUniversity(Long id, University university) {
        universityRepo.findById(id)
                .map(existingUniversity -> {
                    existingUniversity.setUnivNom(university.getUnivNom());
                    return universityRepo.save(existingUniversity);
                })
                .orElseGet(() -> universityRepo.save(university));
    }

}
